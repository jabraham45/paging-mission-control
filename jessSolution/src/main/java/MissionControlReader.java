



import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.StringUtils;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class MissionControlReader {

    public static void main(String[] args) throws JsonProcessingException, ParseException {
        String fileName = "missionControlData.txt";
        ClassLoader classLoader = MissionControlReader.class.getClassLoader();

        InputStream is = MissionControlReader.class.getClassLoader().getResourceAsStream("missionControlData");

        Scanner myReader = new Scanner(is);
        List<MissionControlDTO> battList = new ArrayList<MissionControlDTO>();
        List<MissionControlDTO> tsatList = new ArrayList<MissionControlDTO>();
        while (myReader.hasNextLine()) {
            String data = myReader.nextLine();
            MissionControlDTO missionControlDTO = new MissionControlDTO(data);
            if (StringUtils.containsIgnoreCase(missionControlDTO.getComponent(), "tstat")) {
                tsatList.add(missionControlDTO);
            } else
                battList.add(missionControlDTO);
        }
        myReader.close();

        String format= "yyyyMMdd HH:mm:ss";  // "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS'Z'";
        Comparator<MissionControlDTO> dtoComparator = (MissionControlDTO o1, MissionControlDTO o2) -> {
            try {
                return new SimpleDateFormat(format).parse(o1.getTimestamp()).compareTo(new SimpleDateFormat(format).parse(o2.getTimestamp()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return 0;
        };
        HashMap<Integer, List<MissionControlDTO>> hashMap = new HashMap<Integer, List<MissionControlDTO>>();
        for (MissionControlDTO dto : tsatList) {
            if(!hashMap.containsKey(Integer.valueOf(dto.getSatelliteId()))) {
                List<MissionControlDTO> filtered_tsatList = tsatList.stream().filter(tsat -> tsat.getSatelliteId().contains(dto.getSatelliteId())).collect(Collectors.toList());
                if (filtered_tsatList.size() >= 3) {
                    //sort the lists by date
                    Collections.sort(filtered_tsatList, dtoComparator.reversed());
                    hashMap.put(Integer.valueOf(dto.getSatelliteId()),filtered_tsatList);
                    new OutputDTO(hashMap);
                }
            }
        }

        HashMap<Integer, List<MissionControlDTO>> hashMapBatt = new HashMap<Integer, List<MissionControlDTO>>();
        for (MissionControlDTO dto2 : battList) {
            if(!hashMapBatt.containsKey(Integer.valueOf(dto2.getSatelliteId()))) {
                List<MissionControlDTO> filtered_battList = battList.stream().filter(batt -> batt.getSatelliteId().contains(dto2.getSatelliteId())).collect(Collectors.toList());
                if (filtered_battList.size() >= 3) {
                    //sort the lists by date desc
                    Collections.sort(filtered_battList, dtoComparator.reversed());
                    hashMapBatt.put(Integer.valueOf(dto2.getSatelliteId()),filtered_battList);
                    new OutputDTO(hashMapBatt);
                }
            }

        }

    }

}





