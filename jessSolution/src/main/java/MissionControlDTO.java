import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MissionControlDTO {

    //<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
    public MissionControlDTO(String dataLine){
        super();
        List<String> alist = new ArrayList();
        StringTokenizer st = new StringTokenizer(dataLine, "|");
        while(st.hasMoreTokens()){
            alist.add(st.nextToken());
        }
        this.setTimestamp(alist.get(0));
        this.setSatelliteId(alist.get(1));
        this.setRedHigh(alist.get(2));
        this.setYellowHigh(alist.get(3));
        this.setYellowLow(alist.get(4));
        this.setRedLow(alist.get(5));
        this.setComponent(alist.get(7));
    }


    private String satelliteId;
    private String redHigh;
    private String redLow;
    private String yellowHigh;
    private String yellowLow;
    private String component;
    private String timestamp;



    public String getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }


    public String getRedHigh() {
        return redHigh;
    }

    public void setRedHigh(String redHigh) {
        this.redHigh = redHigh;
    }

    public String getRedLow() {
        return redLow;
    }

    public void setRedLow(String redLow) {
        this.redLow = redLow;
    }

    public String getYellowHigh() {
        return yellowHigh;
    }

    public void setYellowHigh(String yellowHigh) {
        this.yellowHigh = yellowHigh;
    }

    public String getYellowLow() {
        return yellowLow;
    }

    public void setYellowLow(String yellowLow) {
        this.yellowLow = yellowLow;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
