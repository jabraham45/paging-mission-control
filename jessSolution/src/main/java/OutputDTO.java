import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OutputDTO {


    private String satelliteId;
    private String severity;
    private String component;
    private String timestamp;

    public OutputDTO(HashMap<Integer,List<MissionControlDTO>> map) throws JsonProcessingException, ParseException {
        super();
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        for(Map.Entry<Integer, List<MissionControlDTO>> entry:map.entrySet()){
            for(MissionControlDTO dto: entry.getValue()) {
                this.setSatelliteId(dto.getSatelliteId());
                this.setSeverity((StringUtils.containsIgnoreCase(dto.getComponent(), "tstat")) ? "RED HIGH" : "RED LOW");
                this.setComponent(dto.getComponent());
                String format = "yyyyMMdd HH:mm:ss";
//                this.setTimestamp(new SimpleDateFormat(format).parse(dto.getTimestamp())); //date is not formatting correctly
                this.setTimestamp(dto.getTimestamp());//date is not formatting correctly
                String json = mapper.writeValueAsString(this);
                System.out.println(json);
            }
        }


    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

//    public Date getTimestamp() {
//        return timestamp;
//    }
//
//    public void setTimestamp(Date timestamp) {
//        this.timestamp = timestamp;
//    }


    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }
}


